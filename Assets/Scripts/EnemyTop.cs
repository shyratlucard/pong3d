﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTop : MonoBehaviour {

    private Transform playerTransform;
    private Vector3 newPos;

    public Transform ballPosition;

    public float speed;
    private float move;

    // Use this for initialization
    void Start () {

        playerTransform = transform;

        newPos = playerTransform.position;

    }
	
	// Update is called once per frame
	void Update () {

        if (ballPosition.position.x > transform.position.x)
        {
            move = 1;
        }
        else
        {
            move = -1;
        }
        move *= Time.deltaTime;
        move *= speed;
        transform.Translate(move, 0, 0);

        if (transform.position.y < -4.4f)
        {
            transform.position = new Vector3(transform.position.x, -4.4f, transform.position.z);
        }
        else if (transform.position.y > 4.4f)
        {
            transform.position = new Vector3(transform.position.x, 4.4f, transform.position.z);
        }

    }
}
