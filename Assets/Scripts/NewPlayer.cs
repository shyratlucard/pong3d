﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewPlayer : MonoBehaviour {

    private Transform playerTransform;

    private Vector3 newPos;

    public float speed;
    private float move;

    // Use this for initialization
    void Start () {
        playerTransform = transform;

        newPos = playerTransform.position;
    }
	
	// Update is called once per frame
	void Update () {
        move = Input.GetAxis("Horizontal") * speed;
        move *= Time.deltaTime;
        move *= speed;
        transform.Translate(move, 0, 0);

        if (transform.position.x < -10.4f)
        {
            transform.position = new Vector3(-10.4f, transform.position.y, transform.position.z);
        }
        else if (transform.position.x > 10.4f)
        {
            transform.position = new Vector3(10.4f, transform.position.y, transform.position.z);
        }
    }
}
